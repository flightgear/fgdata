<?xml version="1.0" encoding="utf-8"?>
<PropertyList>
  <name>Effects/text-default</name>
  <parameters>
    <!-- shadows.glsl -->
    <show-shadow-cascades>
      <use>/sim/rendering/hdr/debug/show-shadow-cascades</use>
    </show-shadow-cascades>
    <normal-bias>
      <use>/sim/rendering/hdr/shadows/normal-bias</use>
    </normal-bias>
    <sss-enabled>
      <use>/sim/rendering/hdr/shadows/sss-enabled</use>
    </sss-enabled>
    <sss-step-count>
      <use>/sim/rendering/hdr/shadows/sss-step-count</use>
    </sss-step-count>
    <sss-max-distance>
      <use>/sim/rendering/hdr/shadows/sss-max-distance</use>
    </sss-max-distance>
    <sss-depth-bias>
      <use>/sim/rendering/hdr/shadows/sss-depth-bias</use>
    </sss-depth-bias>
    <!-- exposure.glsl -->
    <exposure-compensation>
      <use>/sim/rendering/hdr/exposure-compensation</use>
    </exposure-compensation>
  </parameters>

  <technique n="129">
    <scheme>hdr-forward</scheme>
    <pass>
      <depth>
        <enabled>true</enabled>
        <function>less</function>
        <write-mask>false</write-mask>
      </depth>
      <blend>1</blend>
      <rendering-hint>transparent</rendering-hint>
      <cull-face>back</cull-face>
      <program>
        <vertex-shader>Shaders/HDR/text.vert</vertex-shader>
        <vertex-shader>Shaders/HDR/logarithmic_depth.glsl</vertex-shader>
        <vertex-shader>Shaders/HDR/aerial_perspective.glsl</vertex-shader>
        <fragment-shader>Shaders/HDR/text.frag</fragment-shader>
        <fragment-shader>Shaders/HDR/color.glsl</fragment-shader>
        <fragment-shader>Shaders/HDR/math.glsl</fragment-shader>
        <fragment-shader>Shaders/HDR/logarithmic_depth.glsl</fragment-shader>
        <fragment-shader>Shaders/HDR/shading_transparent.glsl</fragment-shader>
        <fragment-shader>Shaders/HDR/surface.glsl</fragment-shader>
        <fragment-shader>Shaders/HDR/ibl.glsl</fragment-shader>
        <fragment-shader>Shaders/HDR/shadows.glsl</fragment-shader>
        <fragment-shader>Shaders/HDR/sun.glsl</fragment-shader>
        <fragment-shader>Shaders/HDR/aerial_perspective.glsl</fragment-shader>
        <fragment-shader>Shaders/HDR/atmos_spectral.glsl</fragment-shader>
        <fragment-shader>Shaders/HDR/clustered.glsl</fragment-shader>
        <fragment-shader>Shaders/HDR/exposure.glsl</fragment-shader>
      </program>
      <uniform>
        <name>glyph_tex</name>
        <type>sampler-2d</type>
        <value type="int">0</value>
      </uniform>
      <!-- shadows.glsl -->
      <uniform>
        <name>shadow_tex</name>
        <type>sampler-2d-shadow</type>
        <value type="int">10</value>
      </uniform>
      <uniform>
        <name>debug_shadow_cascades</name>
        <type>bool</type>
        <value><use>show-shadow-cascades</use></value>
      </uniform>
      <uniform>
        <name>normal_bias</name>
        <type>float</type>
        <value><use>normal-bias</use></value>
      </uniform>
      <uniform>
        <name>sss_enabled</name>
        <type>bool</type>
        <value><use>sss-enabled</use></value>
      </uniform>
      <uniform>
        <name>sss_step_count</name>
        <type>int</type>
        <value><use>sss-step-count</use></value>
      </uniform>
      <uniform>
        <name>sss_max_distance</name>
        <type>float</type>
        <value><use>sss-max-distance</use></value>
      </uniform>
      <uniform>
        <name>sss_depth_bias</name>
        <type>float</type>
        <value><use>sss-depth-bias</use></value>
      </uniform>
      <!-- ibl.glsl -->
      <uniform>
        <name>dfg_tex</name>
        <type>sampler-2d</type>
        <value type="int">8</value>
      </uniform>
      <uniform>
        <name>prefiltered_envmap_tex</name>
        <type>sampler-cube</type>
        <value type="int">9</value>
      </uniform>
      <!-- aerial_perspective.glsl -->
      <uniform>
        <name>aerial_perspective_tex</name>
        <type>sampler-3d</type>
        <value type="int">11</value>
      </uniform>
      <!-- sun.glsl -->
      <uniform>
        <name>transmittance_tex</name>
        <type>sampler-2d</type>
        <value type="int">12</value>
      </uniform>
      <!-- exposure.glsl -->
      <uniform>
        <name>lum_tex</name>
        <type>sampler-2d</type>
        <value type="int">14</value>
      </uniform>
      <uniform>
        <name>exposure_compensation</name>
        <type>float</type>
        <value><use>exposure-compensation</use></value>
      </uniform>
    </pass>
  </technique>
</PropertyList>
