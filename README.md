# FlightGear base package

The **FlightGear base package** contains the necessary data files to run FlightGear. It consists of the default aircraft, sounds, 3D models, textures, shaders, etc. The base package is also known as `$FG_ROOT` or FGData.

For more information, please go to the [main FlightGear repository](https://gitlab.com/flightgear/flightgear).

## License

The FlightGear base package is licensed under the GNU GPL. For more information, see the [license](LICENSE.md).

## Contact

To get in touch, you can use the GitLab
[issue tracker](https://gitlab.com/groups/flightgear/-/issues). There is also a
[developer mailing list](https://sourceforge.net/projects/flightgear/lists/flightgear-devel).
